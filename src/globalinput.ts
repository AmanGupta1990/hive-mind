import {addWheelListener} from './addWheelListner';
declare var PIXI:any;

export function bindInput (graphics) {
var isDragging = false;
 var  prevX, prevY;
 var nodeUnderCursor=null;
 var previousNode = null;
  var graphGraphics = graphics.graphGraphics;
  var layout = graphics.layout;
  addWheelListener(graphics.domContainer.parentElement, function (e) {
    zoom(e.clientX, e.clientY, e.deltaY < 0);
  },false);

  addDragNDrop();

  var getGraphCoordinates = (function () {
    var ctx = {
      global: { x: 0, y: 0} // store it inside closure to avoid GC pressure
    };

    return function (x, y) {
      ctx.global.x = x; ctx.global.y = y;
      return PIXI.interaction.InteractionData.prototype.getLocalPosition.call(ctx, graphGraphics);
    }
  }());

  function zoom(x, y, isZoomIn) {
   let direction = isZoomIn ? 1 : -1;
    var factor = (1 + direction * 0.1);
    graphGraphics.scale.x *= factor;
    graphGraphics.scale.y *= factor;
    graphGraphics.updateTransform();
  }

  function addDragNDrop() {
    var stage = graphics.stage;
    stage.interactive = true;

         isDragging = false;
      

    stage.mousedown = function (moveData) {
      var pos = moveData.data.global;
       var graphPos = getGraphCoordinates(pos.x, pos.y);
        nodeUnderCursor = graphics.getNodeAt(graphPos.x, graphPos.y);
      if (nodeUnderCursor) {
        // just to make sure layouter will not attempt to move this node
        // based on physical forces. Now it's completely under our control:
        //layout.pinNode(nodeUnderCursor, true);
        graphics.nodeSelected(nodeUnderCursor);
        
      }
      else{
        graphics.nodeSelected(null);
      }
      prevX = pos.x; prevY = pos.y;
      isDragging = true;
    };

    stage.mousemove = function (moveData) {
      if (!isDragging) {
        return;
      }

      var pos = moveData.data.global;

        if (nodeUnderCursor) {
        var graphPos = pos;
       // layout.setNodePosition(nodeUnderCursor.id, graphPos.x, graphPos.y);
      } else {
        var dx = pos.x - prevX;
        var dy = pos.y - prevY;
        prevX = pos.x; prevY = pos.y;
        graphGraphics.position.x += dx;
        graphGraphics.position.y += dy;
      }
    };

    stage.mouseup = function (moveDate) {
      isDragging = false;
    };
  }
}