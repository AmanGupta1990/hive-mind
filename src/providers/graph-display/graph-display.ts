import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import * as ngraph from 'ngraph.graph';
import * as nlayout from 'ngraph.forcelayout';
import * as physics from 'ngraph.physics.simulator'
import { bindInput } from '../../globalinput';

declare var PIXI: any;
declare var TWEEN: any;
/*
  Generated class for the GraphDisplayProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class GraphDisplayProvider {
  private graphicsObj: any;
  private currentNode:any;
  private nodeTiming = 300;
  private tweenFunction = TWEEN.Easing.Cubic.InOut;
  constructor() {
    console.log('Hello GraphDisplayProvider Provider');
  }
  init(displayElement) {
    this.graphicsObj = {};
    var NODE_WIDTH = 10;
    // First we setup PIXI for rendering:
    var stage = new PIXI.Stage(0xFFFFFF, true);
    stage.interactive = true;
    var width = window.innerWidth,
      height = window.innerHeight;
    stage.hitArea = new PIXI.Rectangle(0, 0, width, height);
    stage.buttonMode = true;

    var renderer = PIXI.autoDetectRenderer(width, height, {
      view: document.getElementById(displayElement)
    });
    renderer.view.style.display = "block";
    document.getElementById("renderer").appendChild(renderer.view);

    var graphics = new PIXI.Graphics();
    graphics.position.x = width / 2;
    graphics.position.y = height / 2;
    graphics.scale.x = 1;
    graphics.scale.y = 1;
    stage.addChild(graphics);


    // Second, we create a graph and force directed layout
    window['ng'] = ngraph;
    var graph = ngraph();
    graph.addNode('1', { index: 1 , title:"story 1"});
    graph.addNode('2', { index: 2 , title:"story 2" });
    graph.addNode('3', { index: 3 , title:"story 3" });
    graph.addNode('4', { index: 4 , title:"story 4" });
    graph.addLink('1', '2');
    graph.addLink('2', '3');
    graph.addLink('2', '1');
    graph.addLink('4', '2');
    var simulator = physics({
      springLength: 30,
      springCoeff: 0.0008,
      dragCoeff: 0.01,
      gravity: -1.2,
      theta: 1
    });

    var layout = nlayout(graph, simulator);

    // Store node and link positions into arrays for quicker access within
    // animation loop:
    var nodePositions = [],
      getNodeByIndex = {},
      linkPositions = [],
      nodeSizes = [];

    graph.forEachNode(function (node) {
      nodePositions.push(layout.getNodePosition(node.id));
      nodeSizes.push({ radius: 10 });
      getNodeByIndex[nodePositions.length - 1] = node;
    });

    // Start the tween immediately.

    graph.forEachLink(function (link) {
      linkPositions.push(layout.getLinkPosition(link.id));
    });

    this.graphicsObj = {
      domContainer: renderer.view,
      graphGraphics: graphics,
      stage: stage,
      layout: layout,
      renderer: renderer,
      getNodeAt:  (x, y) =>{
        var half = NODE_WIDTH / 2;
        // currently it's a linear search, but nothing stops us from refactoring
        // this into spatial lookup data structure in future:
        for (var i = 0; i < nodePositions.length; ++i) {
          var pos = nodePositions[i];
          var insideNode = pos.x - half < x && x < pos.x + half &&
            pos.y - half < y && y < pos.y + half;

          if (insideNode) {
            return getNodeByIndex[i];
          }
        }
      },
      nodeSelected: (node)=>{

      if(node == null){
        return;
      }

      if(node){

        if(this.currentNode){
          if(this.currentNode.data.index == node.data.index)
          return;
        }

         let tween1 = new TWEEN.Tween(nodeSizes[node.data.index - 1]) // Create a new tween that modifies 'coords'.
        .to({ radius: 20 }, this.nodeTiming)
        .yoyo( true ) // Move to (300, 200) in 1 second.
        .easing(this.tweenFunction) // Use an easing function to make the animation smooth.
        .onUpdate( ()=> { // Called after tween.js updates 'coords'.

        })
        .start();

        if(this.currentNode){
          if(this.currentNode.data.index != node.data.index){
               let tween2 = new TWEEN.Tween(nodeSizes[this.currentNode.data.index - 1]) // Create a new tween that modifies 'coords'.
        .to({ radius: 10 }, this.nodeTiming )
        .yoyo( true ) // Move to (300, 200) in 1 second.
        .easing(this.tweenFunction) // Use an easing function to make the animation smooth.
        .onUpdate( ()=> { // Called after tween.js updates 'coords'.

        })
        .start();
          }
        }

        this.currentNode = node;
        return;
      }

      
   
      }
    }
    let binding = bindInput(this.graphicsObj)

    // Finally launch animation loop:
    requestAnimationFrame(animate);

    function animate() {
      layout.step();
      drawGraph(graphics, nodePositions, linkPositions);
      renderer.render(stage);
      TWEEN.update();
      requestAnimationFrame(animate);
    }

    function drawGraph(graphics, nodePositions, linkPositions) {
      // No magic at all: Iterate over positions array and render nodes/links
      graphics.clear();
      graphics.beginFill(0xFF5552);
      var i, x, y, x1, y1;

      graphics.lineStyle(1, 0xcccccc, 1);
      for (i = 0; i < linkPositions.length; ++i) {
        var link = linkPositions[i];
        graphics.moveTo(link.from.x, link.from.y);
        graphics.lineTo(link.to.x, link.to.y);
      }

      graphics.lineStyle(0);
      // draw the circles with the text
      for (i = 0; i < nodePositions.length; ++i) {
        x = nodePositions[i].x;
        y = nodePositions[i].y;
        graphics.drawCircle(x, y, nodeSizes[i].radius);
        
        graphics.fillText("Hello World",10,50);
      }
    }

  }

}
