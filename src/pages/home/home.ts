import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {GraphDisplayProvider} from '../../providers/graph-display/graph-display';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[GraphDisplayProvider]
})
export class HomePage {
private displayElement = "graph-container";
  constructor(private graphDisplay:GraphDisplayProvider){

  }

  ngAfterViewInit() {
   window.addEventListener('load',()=>{
     this.graphDisplay.init(this.displayElement);
   })
  }
}
